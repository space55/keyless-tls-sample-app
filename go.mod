module gitlab.com/space55/keyless-tls-sample-app

go 1.18

require (
	github.com/rs/zerolog v1.26.1
	gitlab.com/space55/keyless-tls-terminator v0.0.3
)

require (
	github.com/BurntSushi/toml v1.1.0 // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/juliangruber/go-intersect v1.1.0 // indirect
	github.com/vmihailenco/msgpack/v5 v5.3.5 // indirect
	github.com/vmihailenco/tagparser/v2 v2.0.0 // indirect
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b // indirect
)
