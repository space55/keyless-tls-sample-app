package main

import (
	"net"

	"github.com/rs/zerolog"
	"gitlab.com/space55/keyless-tls-terminator/kttlog"
	"gitlab.com/space55/keyless-tls-terminator/proxy"
)

// Create a sample struct that contains various fields and methods to hold our proxy
type MyTrafficHandler struct {
	// The upstream address we want to proxy to
	UpstreamAddress string

	// The upstream network we want to communicate over
	UpstreamNetwork string
}

func main() {
	kttlog.SetLevel(zerolog.TraceLevel)

	// Define the address to listen on
	listenAddress := ":9443"

	// Listen on TCP
	listenNetwork := "tcp"

	// Define the address of the NoKeyServer
	nokeyserverAddress := "127.0.0.1:7753"
	// Communicate over TCP with the NoKeyServer
	nokeyserverNetwork := "tcp"

	// Define the path of the certificate to use for the proxy to connect to the NKS
	certificatePath := "proxy.pem"

	// Define the path of the key to use for the proxy to connect to the NKS
	keyPath := "proxy.key"

	// Define the path of the certificate authority that signed *both* the proxy certificate and the NKS certificate
	// NOTE: This must be a slice, as there could be multiple CAs that sign NKS server certificates
	caPath := []string{"ca.crt"}

	// Create our MyTrafficHandler struct
	handler := MyTrafficHandler{
		UpstreamAddress: "127.0.0.1:8080", // For example, Nginx running on port 8080
		UpstreamNetwork: "tcp",            // We are communicating with the upstream over TCP
	}

	// Create a new proxy
	tlsProxy, err := proxy.NewTLSProxy(nokeyserverNetwork, nokeyserverAddress, certificatePath, keyPath, caPath, handler.trafficHandler)

	// If there was an issue creating the proxy, panic, and tell us why
	if err != nil {
		panic(err)
	}

	// Define a certificate path that the NKS will hold the private key of. See the next comment for more details
	publicCertificates := []string{"public.pem"}

	// Load the certificates from disk that the NKS will store
	// NOTE: These are *not* the private keys - these are just the public facing certificates.
	// They will be automatically pulled from the NKS in a future release
	err = tlsProxy.LoadRemoteCerts(publicCertificates)

	// If there was an issue loading the certificates, panic, and tell us why
	if err != nil {
		panic(err)
	}

	err = tlsProxy.ListenAndServe(listenNetwork, listenAddress)

	// If there was an serving the proxy, panic, and tell us why
	if err != nil {
		println("A")
		panic(err)
	}
}

func (trafficHandler *MyTrafficHandler) trafficHandler(conn net.Conn) error {
	// Create a new connection to the upstream address
	upstreamConn, err := net.Dial(trafficHandler.UpstreamNetwork, trafficHandler.UpstreamAddress)

	// If there was an error creating the connection, return the error
	if err != nil {
		return err
	}

	// Pipe the two connections together. Return any error they close with
	return proxy.PipeConns(conn, upstreamConn)
}
