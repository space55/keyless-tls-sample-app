# Sample Proxy for Keyless TLS Terminator

This is a sample proxy utilizing the [keyless tls terminator](https://gitlab.com/space55/keyless-tls-terminator) I wrote.

## The Details

### Why?

This is meant to be a barebones example that shows a good amount of functionality that KTT provides, without being over-dense.

### How?

To get this application running, simply download this code and run it. It's pretty simple!

### What about the NoKeyServer?

The NoKeyServer is what handles all of the keys, performing proofs on request. There are more docs about it in the KTT wiki.

## Getting Started

First, clone the repository:

```bash
git clone https://gitlab.com/space55/keyless-tls-sample-app.git
cd keyless-tls-sample-app
```

To get this running, you'll need a few certificates. Usually, these are a pain to generate with openssl. However, [mkcert](https://github.com/FiloSottile/mkcert) is an absolutely awesome way to handle this!

To generate your certificates, here is the process:

```bash
# Create the certificate for the proxy, set to be a TLS client certificate
mkcert --cert-file proxy.crt --key-file proxy.key --client localhost

# Run this in the directory you are going to be running the NKS in
mkcert --cert-file nks.crt --key-file nks.key localhost

# Run this in a directory that the NKS will read from. Generate the public certificate to present to users for the domain localhost
# Then, copy public.crt and store it next to proxy.crt above
mkcert --cert-file public.crt --key-file public.key localhost

# Then, export the CA, so it's stored right next to proxy.crt and proxy.key
cp "$(mkcert --CAROOT)/rootCA.pem" ca.crt
```

### Running the Application

Once your certifiates are generated, just run this:

```bash
go run main.go
```

Or, alternatively:

```bash
go build
./keyless-tls-sample-app
```

## Extending

Feel free to take this code and turn it into something amazing!

## License

This sample app is licensed under the MIT license, available [here](https://gitlab.com/space55/keyless-tls-sample-app/-/blob/main/LICENSE)
